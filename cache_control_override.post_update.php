<?php

/**
 * @file
 * Post updates for Cache Control Override.
 */

/**
 * Clear the entity type cache.
 *
 * New arguments added to service
 * 'cache_control_override.cache_control_override_subscriber'.
 */
function cache_control_override_post_update_service_arguments() {
  // Empty post_update hook.
}
